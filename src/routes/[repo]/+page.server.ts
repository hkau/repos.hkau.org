import type { PageServerLoad } from "./$types";

export const load = (async ({ params }) => {
	return {
		name: params.repo,
		repos: await (await fetch("https://codeberg.org/api/v1/users/hkau/repos")).json(),
		repo: await (await fetch(`https://codeberg.org/api/v1/repos/hkau/${params.repo}`)).json()
	};
}) satisfies PageServerLoad;
