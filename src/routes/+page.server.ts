import type { PageServerLoad } from "./$types";

export const load = (async ({ params }) => {
	return {
        repos: await (await fetch("https://codeberg.org/api/v1/users/hkau/repos")).json()
    };
}) satisfies PageServerLoad;
